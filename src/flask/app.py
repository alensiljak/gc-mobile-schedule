"""
Flask web app
"""

from flask import Flask, render_template, request
app = Flask(__name__)


@app.route('/', methods=['GET'])
def hello():
    """ using a template """
    # return "Hello World!"
    return render_template("index.html")

@app.route("/", methods=['POST'])
def hello_post():
    """ accept some input from the user """
    something = ""
    #if request.args:
    if request.form:
        # model.account_id = request.args.get('account')
        #something = request.args.get('test_entry')
        something = request.form.get('test_entry')

    return f"Got it! {something}"

@app.route("/hello2")
def hello2():
    """ destination for a link """
    return "hi!"

if __name__ == '__main__':
    #app.run(ssl_context='adhoc')
    #app.run()
    app.run(debug=True)
