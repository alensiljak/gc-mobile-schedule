"""
GnuCash Mobile Schedule
"""
import toga
from toga.style.pack import Pack, ROW, CENTER, COLUMN


class GCMobileSchedule(toga.App):
    """ The app """

    def startup(self):
        # Create a main window with a name matching the app
        self.main_window = toga.MainWindow(title=self.name)

        # Create a main content box
        main_box = self.create_ui()

        # Add the content on the main window
        self.main_window.content = main_box

        # Show the main window
        self.main_window.show()

    def create_ui(self):
        """ Create the UI controls """
        box = toga.Box()

        return box


def main():
    return GCMobileSchedule('GnuCash Mobile Schedule', 'ml.alensiljak.gcmobileschedule')

