BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `versions` (
	`table_name`	text ( 50 ) NOT NULL,
	`table_version`	integer NOT NULL,
	PRIMARY KEY(`table_name`)
);
INSERT INTO `versions` VALUES ('Gnucash',3000004);
INSERT INTO `versions` VALUES ('Gnucash-Resave',19920);
INSERT INTO `versions` VALUES ('books',1);
INSERT INTO `versions` VALUES ('commodities',1);
INSERT INTO `versions` VALUES ('accounts',1);
INSERT INTO `versions` VALUES ('budgets',1);
INSERT INTO `versions` VALUES ('budget_amounts',1);
INSERT INTO `versions` VALUES ('prices',3);
INSERT INTO `versions` VALUES ('transactions',4);
INSERT INTO `versions` VALUES ('splits',4);
INSERT INTO `versions` VALUES ('slots',4);
INSERT INTO `versions` VALUES ('recurrences',2);
INSERT INTO `versions` VALUES ('schedxactions',1);
INSERT INTO `versions` VALUES ('lots',2);
INSERT INTO `versions` VALUES ('billterms',2);
INSERT INTO `versions` VALUES ('customers',2);
INSERT INTO `versions` VALUES ('employees',2);
INSERT INTO `versions` VALUES ('entries',4);
INSERT INTO `versions` VALUES ('invoices',4);
INSERT INTO `versions` VALUES ('jobs',1);
INSERT INTO `versions` VALUES ('orders',1);
INSERT INTO `versions` VALUES ('taxtables',2);
INSERT INTO `versions` VALUES ('taxtable_entries',3);
INSERT INTO `versions` VALUES ('vendors',1);
CREATE TABLE IF NOT EXISTS `vendors` (
	`guid`	text ( 32 ) NOT NULL,
	`name`	text ( 2048 ) NOT NULL,
	`id`	text ( 2048 ) NOT NULL,
	`notes`	text ( 2048 ) NOT NULL,
	`currency`	text ( 32 ) NOT NULL,
	`active`	integer NOT NULL,
	`tax_override`	integer NOT NULL,
	`addr_name`	text ( 1024 ),
	`addr_addr1`	text ( 1024 ),
	`addr_addr2`	text ( 1024 ),
	`addr_addr3`	text ( 1024 ),
	`addr_addr4`	text ( 1024 ),
	`addr_phone`	text ( 128 ),
	`addr_fax`	text ( 128 ),
	`addr_email`	text ( 256 ),
	`terms`	text ( 32 ),
	`tax_inc`	text ( 2048 ),
	`tax_table`	text ( 32 ),
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `transactions` (
	`guid`	text ( 32 ) NOT NULL,
	`currency_guid`	text ( 32 ) NOT NULL,
	`num`	text ( 2048 ) NOT NULL,
	`post_date`	text ( 19 ),
	`enter_date`	text ( 19 ),
	`description`	text ( 2048 ),
	PRIMARY KEY(`guid`)
);
INSERT INTO `transactions` VALUES ('f704c4a003214986a8f5659867ebda04','12f6ff8c04e44962aaa71c590dadc25d','','2019-02-01 10:59:00','2019-02-01 09:18:22','Electricity Company');
CREATE TABLE IF NOT EXISTS `taxtables` (
	`guid`	text ( 32 ) NOT NULL,
	`name`	text ( 50 ) NOT NULL,
	`refcount`	bigint NOT NULL,
	`invisible`	integer NOT NULL,
	`parent`	text ( 32 ),
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `taxtable_entries` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`taxtable`	text ( 32 ) NOT NULL,
	`account`	text ( 32 ) NOT NULL,
	`amount_num`	bigint NOT NULL,
	`amount_denom`	bigint NOT NULL,
	`type`	integer NOT NULL
);
CREATE TABLE IF NOT EXISTS `splits` (
	`guid`	text ( 32 ) NOT NULL,
	`tx_guid`	text ( 32 ) NOT NULL,
	`account_guid`	text ( 32 ) NOT NULL,
	`memo`	text ( 2048 ) NOT NULL,
	`action`	text ( 2048 ) NOT NULL,
	`reconcile_state`	text ( 1 ) NOT NULL,
	`reconcile_date`	text ( 19 ),
	`value_num`	bigint NOT NULL,
	`value_denom`	bigint NOT NULL,
	`quantity_num`	bigint NOT NULL,
	`quantity_denom`	bigint NOT NULL,
	`lot_guid`	text ( 32 ),
	PRIMARY KEY(`guid`)
);
INSERT INTO `splits` VALUES ('0544b8576a3f4024903791d5410a6981','f704c4a003214986a8f5659867ebda04','16a25ca43a0e4d1c8a080d1e94659fad','','','n','1970-01-01 00:00:00',0,100,0,1,NULL);
INSERT INTO `splits` VALUES ('229c18c4b226473fb5fd309a5701feb3','f704c4a003214986a8f5659867ebda04','16a25ca43a0e4d1c8a080d1e94659fad','','','n','1970-01-01 00:00:00',0,100,0,1,NULL);
CREATE TABLE IF NOT EXISTS `slots` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`obj_guid`	text ( 32 ) NOT NULL,
	`name`	text ( 4096 ) NOT NULL,
	`slot_type`	integer NOT NULL,
	`int64_val`	bigint,
	`string_val`	text ( 4096 ),
	`double_val`	float8,
	`timespec_val`	text ( 19 ),
	`guid_val`	text ( 32 ),
	`numeric_val_num`	bigint,
	`numeric_val_denom`	bigint,
	`gdate_val`	text ( 8 )
);
INSERT INTO `slots` VALUES (6,'8967a8b9048d4181a08291874770def5','placeholder',4,0,'true',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (7,'413236b568874d9f86812267b91e8ba4','placeholder',4,0,'true',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (8,'19626f20de144c999f7ca27493792e24','placeholder',4,0,'true',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (9,'2e40f63bfffa43fcae5b27b129ef3845','counter_formats',9,0,NULL,NULL,'1970-01-01 00:00:00','338075dd0381442e8dd0b7cd13ee2a76',0,1,NULL);
INSERT INTO `slots` VALUES (10,'2e40f63bfffa43fcae5b27b129ef3845','features',9,0,NULL,NULL,'1970-01-01 00:00:00','2a405f523e414a5d958b5fb78316de1b',0,1,NULL);
INSERT INTO `slots` VALUES (11,'2a405f523e414a5d958b5fb78316de1b','features/ISO-8601 formatted date strings in SQLite3 databases.',4,0,'Use ISO formatted date-time strings in SQLite3 databases (requires at least GnuCash 2.6.20)',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (12,'2e40f63bfffa43fcae5b27b129ef3845','options',9,0,NULL,NULL,'1970-01-01 00:00:00','e2376b6a86c3411d88b05a8fb36dd156',0,1,NULL);
INSERT INTO `slots` VALUES (13,'e2376b6a86c3411d88b05a8fb36dd156','options/Accounts',9,0,NULL,NULL,'1970-01-01 00:00:00','8553d460c32f48c7a9887953a54ba581',0,1,NULL);
INSERT INTO `slots` VALUES (14,'8553d460c32f48c7a9887953a54ba581','options/Accounts/Use Trading Accounts',4,0,'t',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (15,'e2376b6a86c3411d88b05a8fb36dd156','options/Budgeting',9,0,NULL,NULL,'1970-01-01 00:00:00','7bb4518bce69480c830d396c8e89364e',0,1,NULL);
INSERT INTO `slots` VALUES (16,'a153406cb0854432bf7bd913eb15d226','placeholder',4,0,'true',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (17,'18fc20a6bab44291967e05a3348cd1f3','placeholder',4,0,'true',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (18,'f704c4a003214986a8f5659867ebda04','date-posted',10,0,NULL,NULL,'1970-01-01 00:00:00',NULL,0,1,'20190201');
INSERT INTO `slots` VALUES (19,'0544b8576a3f4024903791d5410a6981','sched-xaction',9,0,NULL,NULL,'1970-01-01 00:00:00','1bcc041c39d44306822d2ec31c0bbead',0,1,NULL);
INSERT INTO `slots` VALUES (20,'1bcc041c39d44306822d2ec31c0bbead','sched-xaction/account',5,0,NULL,NULL,'1970-01-01 00:00:00','2522df8faa8a4cdeb62c45f4e910352f',0,1,NULL);
INSERT INTO `slots` VALUES (21,'1bcc041c39d44306822d2ec31c0bbead','sched-xaction/credit-formula',4,0,'amount',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (22,'1bcc041c39d44306822d2ec31c0bbead','sched-xaction/credit-numeric',3,0,NULL,NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (23,'1bcc041c39d44306822d2ec31c0bbead','sched-xaction/debit-formula',4,0,'',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (24,'1bcc041c39d44306822d2ec31c0bbead','sched-xaction/debit-numeric',3,0,NULL,NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (25,'229c18c4b226473fb5fd309a5701feb3','sched-xaction',9,0,NULL,NULL,'1970-01-01 00:00:00','ff46ed4251304426b5a5e44cbc9a592c',0,1,NULL);
INSERT INTO `slots` VALUES (26,'ff46ed4251304426b5a5e44cbc9a592c','sched-xaction/account',5,0,NULL,NULL,'1970-01-01 00:00:00','70cff1c5f36f478c85acf62ce825cc16',0,1,NULL);
INSERT INTO `slots` VALUES (27,'ff46ed4251304426b5a5e44cbc9a592c','sched-xaction/credit-formula',4,0,'',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (28,'ff46ed4251304426b5a5e44cbc9a592c','sched-xaction/credit-numeric',3,0,NULL,NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (29,'ff46ed4251304426b5a5e44cbc9a592c','sched-xaction/debit-formula',4,0,'amount',NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
INSERT INTO `slots` VALUES (30,'ff46ed4251304426b5a5e44cbc9a592c','sched-xaction/debit-numeric',3,0,NULL,NULL,'1970-01-01 00:00:00',NULL,0,1,NULL);
CREATE TABLE IF NOT EXISTS `schedxactions` (
	`guid`	text ( 32 ) NOT NULL,
	`name`	text ( 2048 ),
	`enabled`	integer NOT NULL,
	`start_date`	text ( 8 ),
	`end_date`	text ( 8 ),
	`last_occur`	text ( 8 ),
	`num_occur`	integer NOT NULL,
	`rem_occur`	integer NOT NULL,
	`auto_create`	integer NOT NULL,
	`auto_notify`	integer NOT NULL,
	`adv_creation`	integer NOT NULL,
	`adv_notify`	integer NOT NULL,
	`instance_count`	integer NOT NULL,
	`template_act_guid`	text ( 32 ) NOT NULL,
	PRIMARY KEY(`guid`)
);
INSERT INTO `schedxactions` VALUES ('cddbe988be9e4a83a3d65c46f48cd3a0','Electricity',1,'20190201',NULL,NULL,0,0,0,0,0,0,1,'16a25ca43a0e4d1c8a080d1e94659fad');
CREATE TABLE IF NOT EXISTS `recurrences` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`obj_guid`	text ( 32 ) NOT NULL,
	`recurrence_mult`	integer NOT NULL,
	`recurrence_period_type`	text ( 2048 ) NOT NULL,
	`recurrence_period_start`	text ( 8 ) NOT NULL,
	`recurrence_weekend_adjust`	text ( 2048 ) NOT NULL
);
INSERT INTO `recurrences` VALUES (3,'cddbe988be9e4a83a3d65c46f48cd3a0',1,'month','20190201','none');
CREATE TABLE IF NOT EXISTS `prices` (
	`guid`	text ( 32 ) NOT NULL,
	`commodity_guid`	text ( 32 ) NOT NULL,
	`currency_guid`	text ( 32 ) NOT NULL,
	`date`	text ( 19 ) NOT NULL,
	`source`	text ( 2048 ),
	`type`	text ( 2048 ),
	`value_num`	bigint NOT NULL,
	`value_denom`	bigint NOT NULL,
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `orders` (
	`guid`	text ( 32 ) NOT NULL,
	`id`	text ( 2048 ) NOT NULL,
	`notes`	text ( 2048 ) NOT NULL,
	`reference`	text ( 2048 ) NOT NULL,
	`active`	integer NOT NULL,
	`date_opened`	text ( 19 ) NOT NULL,
	`date_closed`	text ( 19 ) NOT NULL,
	`owner_type`	integer NOT NULL,
	`owner_guid`	text ( 32 ) NOT NULL,
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `lots` (
	`guid`	text ( 32 ) NOT NULL,
	`account_guid`	text ( 32 ),
	`is_closed`	integer NOT NULL,
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `jobs` (
	`guid`	text ( 32 ) NOT NULL,
	`id`	text ( 2048 ) NOT NULL,
	`name`	text ( 2048 ) NOT NULL,
	`reference`	text ( 2048 ) NOT NULL,
	`active`	integer NOT NULL,
	`owner_type`	integer,
	`owner_guid`	text ( 32 ),
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `invoices` (
	`guid`	text ( 32 ) NOT NULL,
	`id`	text ( 2048 ) NOT NULL,
	`date_opened`	text ( 19 ),
	`date_posted`	text ( 19 ),
	`notes`	text ( 2048 ) NOT NULL,
	`active`	integer NOT NULL,
	`currency`	text ( 32 ) NOT NULL,
	`owner_type`	integer,
	`owner_guid`	text ( 32 ),
	`terms`	text ( 32 ),
	`billing_id`	text ( 2048 ),
	`post_txn`	text ( 32 ),
	`post_lot`	text ( 32 ),
	`post_acc`	text ( 32 ),
	`billto_type`	integer,
	`billto_guid`	text ( 32 ),
	`charge_amt_num`	bigint,
	`charge_amt_denom`	bigint,
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `gnclock` (
	`Hostname`	varchar ( 255 ),
	`PID`	int
);
CREATE TABLE IF NOT EXISTS `entries` (
	`guid`	text ( 32 ) NOT NULL,
	`date`	text ( 19 ) NOT NULL,
	`date_entered`	text ( 19 ),
	`description`	text ( 2048 ),
	`action`	text ( 2048 ),
	`notes`	text ( 2048 ),
	`quantity_num`	bigint,
	`quantity_denom`	bigint,
	`i_acct`	text ( 32 ),
	`i_price_num`	bigint,
	`i_price_denom`	bigint,
	`i_discount_num`	bigint,
	`i_discount_denom`	bigint,
	`invoice`	text ( 32 ),
	`i_disc_type`	text ( 2048 ),
	`i_disc_how`	text ( 2048 ),
	`i_taxable`	integer,
	`i_taxincluded`	integer,
	`i_taxtable`	text ( 32 ),
	`b_acct`	text ( 32 ),
	`b_price_num`	bigint,
	`b_price_denom`	bigint,
	`bill`	text ( 32 ),
	`b_taxable`	integer,
	`b_taxincluded`	integer,
	`b_taxtable`	text ( 32 ),
	`b_paytype`	integer,
	`billable`	integer,
	`billto_type`	integer,
	`billto_guid`	text ( 32 ),
	`order_guid`	text ( 32 ),
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `employees` (
	`guid`	text ( 32 ) NOT NULL,
	`username`	text ( 2048 ) NOT NULL,
	`id`	text ( 2048 ) NOT NULL,
	`language`	text ( 2048 ) NOT NULL,
	`acl`	text ( 2048 ) NOT NULL,
	`active`	integer NOT NULL,
	`currency`	text ( 32 ) NOT NULL,
	`ccard_guid`	text ( 32 ),
	`workday_num`	bigint NOT NULL,
	`workday_denom`	bigint NOT NULL,
	`rate_num`	bigint NOT NULL,
	`rate_denom`	bigint NOT NULL,
	`addr_name`	text ( 1024 ),
	`addr_addr1`	text ( 1024 ),
	`addr_addr2`	text ( 1024 ),
	`addr_addr3`	text ( 1024 ),
	`addr_addr4`	text ( 1024 ),
	`addr_phone`	text ( 128 ),
	`addr_fax`	text ( 128 ),
	`addr_email`	text ( 256 ),
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `customers` (
	`guid`	text ( 32 ) NOT NULL,
	`name`	text ( 2048 ) NOT NULL,
	`id`	text ( 2048 ) NOT NULL,
	`notes`	text ( 2048 ) NOT NULL,
	`active`	integer NOT NULL,
	`discount_num`	bigint NOT NULL,
	`discount_denom`	bigint NOT NULL,
	`credit_num`	bigint NOT NULL,
	`credit_denom`	bigint NOT NULL,
	`currency`	text ( 32 ) NOT NULL,
	`tax_override`	integer NOT NULL,
	`addr_name`	text ( 1024 ),
	`addr_addr1`	text ( 1024 ),
	`addr_addr2`	text ( 1024 ),
	`addr_addr3`	text ( 1024 ),
	`addr_addr4`	text ( 1024 ),
	`addr_phone`	text ( 128 ),
	`addr_fax`	text ( 128 ),
	`addr_email`	text ( 256 ),
	`shipaddr_name`	text ( 1024 ),
	`shipaddr_addr1`	text ( 1024 ),
	`shipaddr_addr2`	text ( 1024 ),
	`shipaddr_addr3`	text ( 1024 ),
	`shipaddr_addr4`	text ( 1024 ),
	`shipaddr_phone`	text ( 128 ),
	`shipaddr_fax`	text ( 128 ),
	`shipaddr_email`	text ( 256 ),
	`terms`	text ( 32 ),
	`tax_included`	integer,
	`taxtable`	text ( 32 ),
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `commodities` (
	`guid`	text ( 32 ) NOT NULL,
	`namespace`	text ( 2048 ) NOT NULL,
	`mnemonic`	text ( 2048 ) NOT NULL,
	`fullname`	text ( 2048 ),
	`cusip`	text ( 2048 ),
	`fraction`	integer NOT NULL,
	`quote_flag`	integer NOT NULL,
	`quote_source`	text ( 2048 ),
	`quote_tz`	text ( 2048 ),
	PRIMARY KEY(`guid`)
);
INSERT INTO `commodities` VALUES ('12f6ff8c04e44962aaa71c590dadc25d','CURRENCY','EUR','Euro','978',100,1,'currency','');
INSERT INTO `commodities` VALUES ('246435a985aa41a5b704715fff2b8c02','template','template','template','template',1,0,NULL,'');
CREATE TABLE IF NOT EXISTS `budgets` (
	`guid`	text ( 32 ) NOT NULL,
	`name`	text ( 2048 ) NOT NULL,
	`description`	text ( 2048 ),
	`num_periods`	integer NOT NULL,
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `budget_amounts` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`budget_guid`	text ( 32 ) NOT NULL,
	`account_guid`	text ( 32 ) NOT NULL,
	`period_num`	integer NOT NULL,
	`amount_num`	bigint NOT NULL,
	`amount_denom`	bigint NOT NULL
);
CREATE TABLE IF NOT EXISTS `books` (
	`guid`	text ( 32 ) NOT NULL,
	`root_account_guid`	text ( 32 ) NOT NULL,
	`root_template_guid`	text ( 32 ) NOT NULL,
	PRIMARY KEY(`guid`)
);
INSERT INTO `books` VALUES ('2e40f63bfffa43fcae5b27b129ef3845','56610ef5b3964f8fa24e10ee20b80e31','0cb599b3b4444c1bab370725f539686a');
CREATE TABLE IF NOT EXISTS `billterms` (
	`guid`	text ( 32 ) NOT NULL,
	`name`	text ( 2048 ) NOT NULL,
	`description`	text ( 2048 ) NOT NULL,
	`refcount`	integer NOT NULL,
	`invisible`	integer NOT NULL,
	`parent`	text ( 32 ),
	`type`	text ( 2048 ) NOT NULL,
	`duedays`	integer,
	`discountdays`	integer,
	`discount_num`	bigint,
	`discount_denom`	bigint,
	`cutoff`	integer,
	PRIMARY KEY(`guid`)
);
CREATE TABLE IF NOT EXISTS `accounts` (
	`guid`	text ( 32 ) NOT NULL,
	`name`	text ( 2048 ) NOT NULL,
	`account_type`	text ( 2048 ) NOT NULL,
	`commodity_guid`	text ( 32 ),
	`commodity_scu`	integer NOT NULL,
	`non_std_scu`	integer NOT NULL,
	`parent_guid`	text ( 32 ),
	`code`	text ( 2048 ),
	`description`	text ( 2048 ),
	`hidden`	integer,
	`placeholder`	integer,
	PRIMARY KEY(`guid`)
);
INSERT INTO `accounts` VALUES ('56610ef5b3964f8fa24e10ee20b80e31','Root Account','ROOT','12f6ff8c04e44962aaa71c590dadc25d',100,0,NULL,'','',0,0);
INSERT INTO `accounts` VALUES ('8967a8b9048d4181a08291874770def5','Assets','ASSET','12f6ff8c04e44962aaa71c590dadc25d',100,0,'56610ef5b3964f8fa24e10ee20b80e31','','Assets',0,1);
INSERT INTO `accounts` VALUES ('413236b568874d9f86812267b91e8ba4','Current Assets','ASSET','12f6ff8c04e44962aaa71c590dadc25d',100,0,'8967a8b9048d4181a08291874770def5','','Current Assets',0,1);
INSERT INTO `accounts` VALUES ('70cff1c5f36f478c85acf62ce825cc16','Checking Account','BANK','12f6ff8c04e44962aaa71c590dadc25d',100,0,'413236b568874d9f86812267b91e8ba4','','Checking Account',0,0);
INSERT INTO `accounts` VALUES ('b1bbffd2c652476585ab172fa6a619b1','Income','INCOME','12f6ff8c04e44962aaa71c590dadc25d',100,0,'56610ef5b3964f8fa24e10ee20b80e31','','Income',0,0);
INSERT INTO `accounts` VALUES ('a153406cb0854432bf7bd913eb15d226','Expenses','EXPENSE','12f6ff8c04e44962aaa71c590dadc25d',100,0,'56610ef5b3964f8fa24e10ee20b80e31','','Expenses',0,1);
INSERT INTO `accounts` VALUES ('19626f20de144c999f7ca27493792e24','Equity','EQUITY','12f6ff8c04e44962aaa71c590dadc25d',100,0,'56610ef5b3964f8fa24e10ee20b80e31','','Equity',0,1);
INSERT INTO `accounts` VALUES ('18fc20a6bab44291967e05a3348cd1f3','Opening Balances','EQUITY','12f6ff8c04e44962aaa71c590dadc25d',100,0,'19626f20de144c999f7ca27493792e24','','Opening Balances',0,1);
INSERT INTO `accounts` VALUES ('0cb599b3b4444c1bab370725f539686a','Template Root','ROOT',NULL,0,0,NULL,'','',0,0);
INSERT INTO `accounts` VALUES ('16a25ca43a0e4d1c8a080d1e94659fad','cddbe988be9e4a83a3d65c46f48cd3a0','BANK','246435a985aa41a5b704715fff2b8c02',1,0,'0cb599b3b4444c1bab370725f539686a','','',0,0);
INSERT INTO `accounts` VALUES ('2522df8faa8a4cdeb62c45f4e910352f','Electricity','EXPENSE','12f6ff8c04e44962aaa71c590dadc25d',100,0,'a153406cb0854432bf7bd913eb15d226','','',0,0);
CREATE INDEX IF NOT EXISTS `tx_post_date_index` ON `transactions` (
	`post_date`
);
CREATE INDEX IF NOT EXISTS `splits_tx_guid_index` ON `splits` (
	`tx_guid`
);
CREATE INDEX IF NOT EXISTS `splits_account_guid_index` ON `splits` (
	`account_guid`
);
CREATE INDEX IF NOT EXISTS `slots_guid_index` ON `slots` (
	`obj_guid`
);
COMMIT;
